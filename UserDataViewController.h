//
//  UserDataViewController.h
//  CPMiOS
//
//  Created by Michael on 5/11/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Reachability.h"

@interface UserDataViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>

{
    NSInteger eyeColorRow, loadedColorRow;
    NSInteger loadedMonth, loadedDay, loadedYear;
    NSString *loadedEyeColor;
    NSString *loadedDOB;
    NSString *selectedDOB;
    NSString *SecretKey;
    NSString *fStr;
    NSString *ma;
    NSString *da;
    NSString *ya;
    NSInteger d;
    NSInteger m;
    NSInteger y;
    BOOL networkConnected;
}


@end
