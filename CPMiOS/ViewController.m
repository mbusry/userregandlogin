//
//  ViewController.m
//  CPMiOS
//
//  Created by Michael on 5/11/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import "ViewController.h"
#import "UserDataViewController.h"
#import "UserRegistrationViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *UserNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *PasswordTextField;

@property (weak, nonatomic) IBOutlet UILabel *errorMessageLabel;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
//    [PFUser enableAutomaticUser];
    PFUser *user = [PFUser currentUser];
    if (user) {
        [PFUser logOut];

        NSLog(@"current user: %@",user);
            //logged in go to the account screen
        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"userstoryboard"];
        [[self navigationController]pushViewController:vc animated:true];
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)userLogin:(id)sender {
    [self networkConnection];
    
    NSLog(@"userLogin Clicked");
    userName = self.UserNameTextField.text;
    userPassword = self.PasswordTextField.text;
    
    [PFUser enableAutomaticUser];
        [PFUser logInWithUsernameInBackground:userName password:userPassword
                                        block:^(PFUser *user, NSError *error) {
                                            if (user) {
                                                    // Do stuff after successful login.
                                                NSLog(@"loggedin - leaving vc.m");
                                                UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                
                                                UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"userstoryboard"];
                                                [[self navigationController]pushViewController:vc animated:true];
//
                                            } else {
                                                    // The login failed. Check error to see why.
                                                NSLog(@"loggedin FAILED");
                                                self.errorMessageLabel.text = @"Error logging in.  Please try again.";

                                                
                                            }
                                        }];
}

-(void)networkConnection{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == ReachableViaWWAN) {
        
            //Code when there is a WAN connection
        networkConnected = TRUE;
        
    } else if (networkStatus == ReachableViaWiFi) {
        
            //Code when there is a WiFi connection
        networkConnected = TRUE;
        
        
    } else if (networkStatus == NotReachable) {
        
            //Code when there is no connection
        networkConnected = FALSE;
        
        
    }
    
    NSLog (@"Value of my BOOL = %@", networkConnected ? @"YES": @"NO");
    
}


@end
