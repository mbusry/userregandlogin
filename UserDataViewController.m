//
//  UserDataViewController.m
//  CPMiOS
//
//  Created by Michael on 5/11/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import "UserDataViewController.h"

@interface UserDataViewController ()

@property (weak, nonatomic) IBOutlet UITextField *userDataUserNameTextField;

@property (weak, nonatomic) IBOutlet UITextField *userDataSecretKeyTextField;

@property (weak, nonatomic) IBOutlet UITextField *userDataDOBTextField;

@property (strong, nonatomic) NSArray *eyeColorArray;

@property (weak, nonatomic) IBOutlet UILabel *errorMessageLabel;

@property (strong, nonatomic) NSString *selectedEyeColor;

@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@property (nonatomic, strong) NSString *currentUser;

@property (weak, nonatomic) IBOutlet UIDatePicker *DOBPicker;


    

@end

@implementation UserDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"userdatavc");
    networkConnected = FALSE;
    [PFUser enableAutomaticUser];
    PFUser *currentUser = [PFUser currentUser];
//    [currentUser fetch];
    
    if (currentUser) {
        NSLog(@"loggedin");
    }else{
        NSLog(@"Not loggedin");
    }
    NSLog(@"UDVC: currentuser %@", currentUser);

    // Do any additional setup after loading the view.
    loadedMonth = 1;
    loadedDay = 1;
    loadedYear = 1980;
    
    self.userDataSecretKeyTextField.delegate = self;
    self.userDataDOBTextField.delegate = self;
    
    
        
        // set eyecolor object
    self.eyeColorArray = [[NSArray alloc] initWithObjects:@"NONE",@"Blue",@"Green",@"Hazel",@"Brown",@"Black",nil];
    

    if (currentUser) {
        
        NSLog(@"currentUser");
        
            //check if secretkey is blank and set the textfield.
        if (currentUser[@"secretkey"] == NULL) {
            self.userDataSecretKeyTextField.text = @"";

        }else{
            self.userDataSecretKeyTextField.text = currentUser[@"secretkey"];

        }
        
        if (currentUser[@"month"] == NULL) {
            loadedMonth = 1;
        }else{
            loadedMonth = [[currentUser objectForKey:@"month"]intValue];
            NSLog(@"loadedMonth %ld",(long)loadedMonth);
        }
        
        if (currentUser[@"day"] == NULL) {
            loadedDay = 1;
        }else{
            loadedDay = [[currentUser objectForKey:@"day"]intValue];
            NSLog(@"loadedDay %ld",(long)loadedDay);

        }

        if (currentUser[@"year"] == NULL) {
            loadedYear = 1980;
        }else{
            loadedYear = [[currentUser objectForKey:@"year"]intValue];
            NSLog(@"loadedYear %ld",(long)loadedYear);

        }
        
     
            // do stuff with the user
        self.userDataUserNameTextField.text = currentUser.username;
        self.userDataDOBTextField.text = currentUser[@"dateofbirth"];
        
        
        if (!currentUser[@"key"]) {  //key is blank
            PFObject *post = currentUser[@"key"];
            NSLog(@"post: %@",post);

            NSLog(@"!post");
            [post fetchIfNeededInBackgroundWithBlock:^(PFObject *post, NSError *error) {
                loadedColorRow = [[post objectForKey:@"colorrow"] integerValue];
                
                NSLog(@"post:eyecolor: %@",post[@"color"]);
                NSLog(@"post:eyecolorrow: %ld",loadedColorRow);
                
                
            }];

            NSLog(@"eyecolor: %@",post[@"color"]);
            NSLog(@"eyecolorrow: %ld",loadedColorRow);



            NSString *eyecolor = post[@"color"];
//            loadedColorRow = post[@colorrow];
                // do something with your title variable
            NSLog(@"eyecolor: %@",eyecolor);
//        }];
        

        

        
        
    } else {
            // show the signup or login screen
        [PFUser enableAutomaticUser];
        PFUser *currentUser = [PFUser currentUser];
//        [currentUser fetch];
        NSLog(@"UDVC else: currentuser %@", currentUser);

    }
 
    
}
}

- (void)viewWillAppear:(BOOL)animated{
    
    [PFUser enableAutomaticUser];
    PFUser *currentUser = [PFUser currentUser];
//    [currentUser fetch];
    PFObject *post = currentUser[@"key"];
    [post fetchIfNeededInBackgroundWithBlock:^(PFObject *post, NSError *error) {
            //int score = [[gameScore objectForKey:@"score"] intValue];
        loadedColorRow = [[post objectForKey:@"colorrow"] integerValue];
        
        NSLog(@"viewWillAppear:eyecolor: %@",post[@"color"]);
        NSLog(@"viewWillAppear:eyecolorrow: %ld",loadedColorRow);
        
        eyeColorRow = loadedColorRow;
        
        [self.picker selectRow:loadedColorRow inComponent:0 animated:YES];
        
        
    }];

    
    NSLog(@"UDVC viewwillappear: currentuser %@", currentUser);
    
    
    loadedDOB = currentUser[@"dateofbirth"];
    NSLog(@"loadedDOB: %@",loadedDOB);

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:loadedDOB];
    
    [_DOBPicker setDate:dateFromString];



    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

    // returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
}

    // returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return self.eyeColorArray.count;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [self.eyeColorArray objectAtIndex:row];
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    eyeColorRow = row;

    NSLog(@"eyeColor: %@",self.eyeColorArray[row]);
    NSLog(@"eyeColorRow: %ld",(long)row);

    
    self.selectedEyeColor = self.eyeColorArray[row];

    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)userUpdate:(id)sender {
    NSLog(@"userUpdate");
    
//     NSString *dateStr = [[NSDate date] description];
    if (selectedDOB) {
        fStr = (NSString *)[[selectedDOB componentsSeparatedByString:@" "]objectAtIndex:0];
        ma = (NSString *)[[fStr componentsSeparatedByString:@"-"]objectAtIndex:0];
        da = (NSString *)[[fStr componentsSeparatedByString:@"-"]objectAtIndex:1];
        ya = (NSString *)[[fStr componentsSeparatedByString:@"-"]objectAtIndex:2];
        d = ([da intValue]);
        m = ([ma intValue]);
        y = ([ya intValue]);
        
        NSLog(@"month: %ld",m);
        NSLog(@"day: %ld",d);
        NSLog(@"year: %ld",y);

    }else{
        NSLog(@"DOB did not change!");
        d = loadedDay ;
        m = loadedMonth;
        y = loadedYear;
    
    }
    


    NSLog(@"selectedEyeColor: %@",self.eyeColorArray[eyeColorRow]);
    NSLog(@"loadedColorRow: %ld",loadedColorRow);
    NSLog(@"selectedEyeColorRow: %ld",eyeColorRow);
    NSLog(@"selectedDOB: %@",selectedDOB);
    NSLog(@"secretkey: %@",self.userDataSecretKeyTextField.text);
    SecretKey = self.userDataSecretKeyTextField.text;
    NSLog(@"secretkey: %@",SecretKey);


        //eyecolor object
    PFObject *ecolor = [PFObject objectWithClassName:@"eyecolor"];
    ecolor[@"color"] = self.eyeColorArray[eyeColorRow];
    [ecolor setObject:[NSNumber numberWithInteger:eyeColorRow] forKey:@"colorrow"];
    [ecolor pinInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
                // The object has been saved.
            NSLog(@"ecolor has been saved");
        } else {
                // There was a problem, check error.description
            NSLog(@"ERROR: %@ ",error.description);
        }
    }];

        //user object
    [PFUser enableAutomaticUser];
    PFUser *user = [PFUser currentUser];
    NSLog(@"user objected selected");
    
        // other fields can be set just like with PFObject
    if (!self.userDataSecretKeyTextField.text) {
        user[@"secretkey"] = @"";
        NSLog(@"secretkey is blank");
    }else{
        NSLog(@"SecretKey = %@",SecretKey);
        user[@"secretkey"] = SecretKey;
    }
    
    if (selectedDOB) {
        user[@"dateofbirth"] = selectedDOB;
        NSLog(@"selectedDOB = %@",selectedDOB);
    }else{
        user[@"dateofbirth"] = loadedDOB;
        NSLog(@"loadedDOB = %@",loadedDOB);

    }
    
    [user setObject:[NSNumber numberWithInteger:d] forKey:@"day"];
    [user setObject:[NSNumber numberWithInteger:m] forKey:@"month"];
    [user setObject:[NSNumber numberWithInteger:y] forKey:@"year"];

    
    NSLog(@"month: %ld",m);
    NSLog(@"day: %ld",d);
    NSLog(@"year: %ld",y);

    
        //relationship
    user[@"key"] = ecolor;
    
    [user saveEventually];
    
}

- (IBAction)userClear:(id)sender {
    NSLog(@"userClear");
    
    self.userDataUserNameTextField.text = @"";
    self.userDataSecretKeyTextField.text = @"";
//    self.userDataDOBTextField.text = @"";
    
    [self.userDataUserNameTextField becomeFirstResponder];



}
- (IBAction)userLogout:(id)sender {
    NSLog(@"userLogout");
    [self networkConnection];
    
    if (networkConnected) {
        NSLog(@"networkConnected");
 
    [PFUser logOut];
//        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        
//        UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"mainstoryboard"];
//        
//        [self presentViewController:vc animated:YES completion:nil];

    }
    
  

    


}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

    // It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)selectRow:(NSInteger)row inComponent:(NSInteger)component animated:(BOOL)animated{
    NSLog(@"selectedRowInComponent: %ld",(long)loadedColorRow);
 
    [self.picker selectRow:loadedColorRow inComponent:0 animated:YES];

}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    NSLog(@"clear button pressed");
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Are your sure?"
                          message:@"Do you really want to delete this?"
                          delegate:self
                          cancelButtonTitle:@"Cancel"
                          otherButtonTitles:@"OK",nil];
    [alert show];

    return NO;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
                //this is the "Cancel"-Button
                //do something
            
        }
            break;
            
        case 1:
        {
                //this is the "OK"-Button
                //do something
            
            PFUser *currentUser = [PFUser currentUser];
            
            [currentUser removeObjectForKey:@"secretkey"];
            [currentUser saveEventually];
            self.userDataSecretKeyTextField.text = @"";

        }
            break;
            
        default:
            break;
    }
    
}

- (IBAction)dobPickerAction:(id)sender {
    

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    

    selectedDOB = [dateFormatter stringFromDate:self.DOBPicker.date];

    
    NSLog(@"selectedDOB: %@", selectedDOB);
        //    self.selectedDate.text =formatedDate;
}

-(void)networkConnection{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == ReachableViaWWAN) {
        
            //Code when there is a WAN connection
        networkConnected = TRUE;
        
    } else if (networkStatus == ReachableViaWiFi) {
        
            //Code when there is a WiFi connection
        networkConnected = TRUE;
        
        
    } else if (networkStatus == NotReachable) {
        
            //Code when there is no connection
        networkConnected = FALSE;
        
        
    }
    
    NSLog (@"Network Connected? %@", networkConnected ? @"YES": @"NO");
    
}



@end
