//
//  UserRegistrationViewController.m
//  CPMiOS
//
//  Created by Michael on 5/11/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import "UserRegistrationViewController.h"

@interface UserRegistrationViewController ()

@end

@implementation UserRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.userNameTextField.delegate = self;
    self.userPasswordTextField.delegate = self;
    self.rePasswordTextField.delegate = self;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)saveUserInfo:(id)sender {
    
    
    NSLog(@"saveUserInfo Clicked");
    
    NSString *un = self.userNameTextField.text;
    NSString *pw = self.userPasswordTextField.text;
    NSString *pw2 = self.rePasswordTextField.text;
    
    if ([pw isEqualToString:pw2]) {
        [PFUser enableAutomaticUser];
        PFUser *user = [PFUser user];
        user.username = un;
        user.password = pw;
        
            // other fields can be set just like with PFObject
        
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {   // Hooray! Let them use the app now.
            } else {   NSString *errorString = [error userInfo][@"error"];   // Show the errorString somewhere and let the user try again.
                
                NSLog(@"Error: %@",errorString);
                self.errorMessageLabel.text = errorString;
            }
        }];
        
    }else{

        self.errorMessageLabel.text = @"Passwords do not match";
        self.userPasswordTextField.text = @"";
        self.rePasswordTextField.text = @"";
        [self.userPasswordTextField becomeFirstResponder];
        [self.rePasswordTextField setReturnKeyType:UIReturnKeyDone];


        
    }

    

    
}
- (IBAction)clearUserInfo:(id)sender {
    
    NSLog(@"clearUserInfo Clicked");
    
self.userNameTextField.text = @"";
self.userPasswordTextField.text = @"";
self.rePasswordTextField.text = @"";
    
    [self.userNameTextField becomeFirstResponder];



}
- (IBAction)userLogout:(id)sender {
    
    [PFUser logOut];
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"mainstoryboard"];
    
    [self presentViewController:vc animated:YES completion:nil];

    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

    // It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



@end
